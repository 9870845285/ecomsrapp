package com.info.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.info.entity.AppUser;
import com.info.service.IAppUserService;

@Controller
@RequestMapping("/appuser")

public class AppUserController {
	@Autowired
	private IAppUserService service;
	
	//1. show Register page
	@GetMapping("/register")
	public String showReg() {
		return "AppUserRegister";
	}
	
	//2. save User
	@PostMapping("/save")
	public String saveUser(
			@ModelAttribute AppUser appUser,
			Model model
			) 
	{
		Long id = service.saveAppUser(appUser);
		model.addAttribute("message", "User '"+id+"' is created");
		return "AppUserRegister";
	}
	//3. show data
	@GetMapping("/all")
	public String showData(Model model) {
		model.addAttribute("list", service.getAllAppUser());
		return "AppUserData";
	}

}
