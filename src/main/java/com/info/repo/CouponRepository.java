package com.info.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.info.entity.Coupon;

public interface CouponRepository extends JpaRepository<Coupon, Long>{

}
