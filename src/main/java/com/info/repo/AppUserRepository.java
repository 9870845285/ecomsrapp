package com.info.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.info.entity.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

}
