package com.info.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.info.entity.Shipping;

public interface ShippingRepository extends JpaRepository<Shipping, Long>{

}
