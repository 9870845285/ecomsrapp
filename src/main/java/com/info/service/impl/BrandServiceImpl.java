package com.info.service.impl;


import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.info.entity.Brand;
import com.info.repo.BrandRepository;
import com.info.service.IBrandService;
import com.info.util.AppUtil;

@Service
public class BrandServiceImpl implements IBrandService{
	@Autowired
	private BrandRepository repo;
	
	public Long saveBrand(Brand b) {
		return repo.save(b).getId();
	}

	public void updateBrand(Brand b) {
		repo.save(b);
	}

	public void deleteBrand(Long id) {
		repo.deleteById(id);
	}

	public Brand getOneBrand(Long id) {
		Optional<Brand>  opt = repo.findById(id);
		if(opt.isPresent()) return opt.get();
		return null;
	}

	public List<Brand> getAllBrands() {
		return repo.findAll();
	}

	@Override
	public Map<Long, String> getBrandIdAndName() {
		List<Object[]> list = repo.getBrandIdAndName();
		//return AppUtil.convertListToMap(list);
		return null;
	}
	
	
}
