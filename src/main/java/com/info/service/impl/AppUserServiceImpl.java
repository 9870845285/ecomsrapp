package com.info.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.info.entity.AppUser;
import com.info.repo.AppUserRepository;
import com.info.service.IAppUserService;
@Service
public class AppUserServiceImpl implements IAppUserService {

	@Autowired
	private AppUserRepository repo;
	
	public Long saveAppUser(AppUser user) {
		return repo.save(user).getId();
	}

	public List<AppUser> getAllAppUser() {
		return repo.findAll();
	}


}
