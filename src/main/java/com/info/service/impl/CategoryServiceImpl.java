package com.info.service.impl;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.info.entity.Category;
import com.info.repo.CategoryRepository;
import com.info.service.ICategoryService;
import com.info.util.AppUtil;
@Service
public class CategoryServiceImpl implements ICategoryService {
	@Autowired
	private CategoryRepository repo;

	@Override
	@Transactional
	public Long saveCategory(Category category) {
		return repo.save(category).getId();
	}

	@Override
	@Transactional
	public void updateCategory(Category category) {
		repo.save(category);
	}

	@Override
	@Transactional
	public void deleteCategory(Long id) {
		repo.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public Category getOneCategory(Long id) {
		return repo.findById(id).get();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Category> getAllCategorys() {
		return repo.findAll();
	}
	
	@Override
	public Map<Long, String> getCategoryIdAndName(String status) {
		List<Object[]> list = repo.getCategoryIdAndName(status);
		//return list.stream().collect(Collectors.toMap(ob->Integer.valueOf(ob[0]),ob->ob[1].toSting()));
	return null;
	}

}
