package com.info.service;

import java.util.List;

import com.info.entity.AppUser;

public interface IAppUserService {
	Long saveAppUser(AppUser user);
	List<AppUser> getAllAppUser();

}
